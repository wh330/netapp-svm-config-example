This page shows how to set up NetApp SVM, create snapmirror relationship and sample NFS shares.

- Create source and destination SVMs
```
cluster1::> vserver create -vserver fps-pvs -subtype default -rootvolume fps_pvs_root -rootvolume-security-style unix -language C.UTF-8 -snapshot-policy none -data-services data-nfs -allowed-protocols nfs -foreground true

cluster2::> vserver create -vserver fps-pvs-dr -subtype dp-destination -data-services data-nfs -foreground true
```

- Create SVM peer relationship
```
cluster1::> vserver peer create -vserver fps-pvs -peer-vserver fps-pvs-dr -applications snapmirror -peer-cluster cluster2

cluster2::> vserver peer accept -vserver fps-pvs-dr -peer-vserver fps-pvs
```

- Configure snapmirror relationship
```
cluster2::> snapmirror create -source-path fps-pvs: -destination-path fps-pvs-dr: -type DP -schedule daily
```

- Create volumes on source SVM
```
cluster1::> volume create -vserver fps-pvs -volume dev -size 70gb -aggregate aggr_10t_01 -state online -policy default -unix-permissions ---rwxr-xr-x -type RW -snapshot-policy none -foreground true
```

- Mount the created volumes
```
cluster1::> volume mount -vserver fps-pvs -volume dev -junction-path /dev

cluster1::> volume mount -vserver fps-pvs -volume live -junction-path /live
```

- Create NFS server on source SVM
```
cluster1::> nfs create -vserver fps-pvs
```

- Create an NFS network interface
```
cluster1::> network interface create -vserver fps-pvs -lif fps-pvs-nfs -service-policy default-data-files -role data -data-protocol nfs -subnet-name <network-subnet>
```

- Add a rule to the default export policy
```
cluster1::> export-policy rule create -vserver fps-pvs -policyname default -clientmatch <addresses> -rorule any -rwrule any -allow-suid true -allow-dev true -superuser any -protocol nfs
```

- Create Qtree for the NFS shares
```
cluster1::> qtree create -vserver fps-pvs -volume dev -qtree fps -security-style unix -oplock-mode enable -unix-permissions ---rwxr-xr-x -export-policy default

cluster1::> qtree create -vserver fps-pvs -volume live -qtree fps -security-style unix -oplock-mode enable -unix-permissions ---rwxr-xr-x -export-policy default
```

- Initialise snapmirror relationship
```
cluster2::> snapmirror initialize -destination-path fps-pvs-dr: -source-path fps-pvs:
```
